---?color=linear-gradient(to right, #c02425, #f0cb35)
@title[Introduction]

@snap[west headline text-white span-70]
Background Simulation<br>*Muon Detectors*
@snapend

@snap[south-west byline  text-white]
BRIL simulation group.
@snapend

---
@title[Slide Markdown]

### Overview of simulation strategy.

<br><br>
1. Select suitable FLUKA-CMS geometry
1. Obtain expected particle fluxes and energy spectra (according to sub-detector volume)
1. Modeling detector response (sensitivity)
1. Comparison with experimental measurements (Bkg HitRate)
<br><br>

---

@title[Previous experience]


#### Previous comparisons 


![Previous results](http://castaned.web.cern.ch/castaned/RE1213_HitRate_Run2.png)


---

@title[Improvements]

#### Improvements to be implemented 

<br><br>
1. Accurate description of energy spectra for each chamber 
2. Improved detector geometry description
<br>